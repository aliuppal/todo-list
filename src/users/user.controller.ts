/**
 * Users Controller
 */

import { UserEntity } from './entity/user';
import { CreateUserDto } from './dto/user.dto';
import { UsersService } from './user.service';
import { Request, Response, NextFunction } from 'express';

export class UserController {

    public usersService = new UsersService();

    public usersFind = async (_request: Request, response: Response, _next: NextFunction) => {
        const users: UserEntity[] = await this.usersService.getAllUsers();
        response.status(200).send(users);
    };

    public userById = async (request: Request, response: Response, next: NextFunction) => {
        const id = +request.params.id;
        const users: UserEntity | undefined = await this.usersService.getUserById(id);
        if (users) {
            response.status(200).send(users);
        } else {
            next(new Error('Error while updating user'));
        }
    };

    public userUpdate = async (request: Request, response: Response, next: NextFunction) => {
        const id: number = +request.params.id;
        const userData: CreateUserDto = request.body;
        const user = await this.usersService.updateUser(id, userData);
        if (user) {
            response.status(200).send(user);
        } else {
            next(new Error('Error Occured while updating user'));
        }
    };

    public userDelete = async (request: Request, response: Response, next: NextFunction) => {
        const id: number = +request.params.id;
        const successResponse = await this.usersService.deleteUserById(id);
        if (successResponse) {
            response.status(200).send('User deleted successfully');
        } else {
            next(new Error('Failed to delete user'));
        }
    };
}
