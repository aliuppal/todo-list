/**
 * Users Services
 */
import { UserEntity } from './entity/user';
import { logger } from '../custom';
import { DeleteResult, getManager, Repository, UpdateResult } from 'typeorm';
import { CreateUserDto } from './dto/user.dto';
import { UserNotFoundException } from '../custom/error-middleware';
import { TaskService } from '../tasks/task.service';
import smtpTransport from '../custom/smtp';
import { TasksEntity } from '../tasks/entity/task';

export class UsersService {
    userRepository: Repository<UserEntity>;
    public taskService = new TaskService();

    constructor() {
        this.userRepository = getManager().getRepository(UserEntity);
    }

    /** Inserts a new User into the database. */
    async createUser(userDto: CreateUserDto): Promise<CreateUserDto> {
        logger.info('Create a new user', userDto);
        const user: any = await this.getUsersByEmail(userDto.email);
        if (user) {
            throw new Error('The User With Email Arleady Exist');
        } else {
            const newUser = this.userRepository.create(userDto);
            return await this.userRepository.save(newUser);
        }
    }

    /** Returns array of all users from db */
    async getAllUsers(): Promise<UserEntity[]> {
        return await this.userRepository.find({ where: { isActive: true, isDeleted: false } });
    }

    /** Returns a user by given id */
    async getUserById(id: number): Promise<UserEntity | any> {
        logger.info('Fetching user by id: ', id);
        if (id) {
            const user = await this.userRepository.findOne({ where: { id: id } });
            return user;
        }
        return Promise.reject(false);
    }

    /** Returns a user by email */
    public async getUsersByEmail(email: string) {
        const user = await this.userRepository.findOne({ where: { isActive: true, isDeleted: false, email: email } });
        return user;
    }

    /** Returns a user by email */
    async getUserByEmail(email: string): Promise<UserEntity | undefined> {
        const users = await this.userRepository.findOne({
            where: { isActive: true, isDeleted: false, email: email },
        });
        if (!users) {
            throw new Error('The user does not exist');
        } else {
            return users;
        }
    }

    /** Updates a user */
    async updateUser(id: number, user: CreateUserDto): Promise<UpdateResult | void> {
        try {
            await this.userRepository.update(id, user);
        } catch (error) {
            throw new UserNotFoundException();
        }
        return;
    }

    /** Returns a user by email Verification Token */
    public async getUserByEmailVerificationToken(userId: number, verificationToken: string) {
        return await this.userRepository.findOne({ where: { isActive: true, isDeleted: false, id: userId, verificationToken } });
    }

    /** Returns a user by Reset Password Token */
    public async getUserByResetPasswordToken(userId: number, resetPasswordToken: string) {
        return await this.userRepository.findOne({ where: { isActive: true, isDeleted: false, id: userId, resetPasswordToken } });
    }

    /** delete user by id */
    async deleteUserById(id: number): Promise<DeleteResult> {
        const user = await this.userRepository.delete(id);
        return user;
    }

    /** generate Users Daily Repost */
    async generateUsersDailyReport(): Promise<void> {
        const users = await this.getAllUsers();
        users.forEach(async (user) => {
            const tasks = await this.taskService.tasksDueToday(user.id)
            if (tasks.length) {
                await this.sendUsersDailyReportEmail(user.email, tasks);
            }
        })
    }

    public async sendUsersDailyReportEmail(email: string, tasks: TasksEntity[]) {
        try {
            const data = {
                to: email,
                from: process.env.No_REPLY_EMAIL,
                template: 'daily-due-tasks-report',
                subject: `Daily due tasks report! `,
                context: {
                    tasks,
                },
            };
            smtpTransport.sendMail(data);

        } catch (err) {
            logger.error(`Exception occurred for sendAccountCreationEmail,-> Error [${err}]`);
        } finally {
            smtpTransport.close();
        }
    }
}
