import { UserController } from './user.controller';
import { Auth } from '../custom/auth';
import userValMiddleware from './user.middleware';
import { errorMiddleware } from '../custom/error-middleware';
import { Application } from 'express';

const userController = new UserController();
const authMiddleware = new Auth();

const resource = '/users';

module.exports = (app: Application, version: string) => {

    app.get(
        `${version}${resource}/results`,
        authMiddleware.verifyUser,
        userController.usersFind
    );
    app.get(
        `${version}${resource}/find/:id`,
        authMiddleware.verifyUser,
        userController.userById
    );
    app.patch(
        `${version}${resource}/update/:id`,
        authMiddleware.verifyUser,
        userController.userUpdate
    );
    app.delete(
        `${version}${resource}/delete/:id`,
        authMiddleware.verifyUser,
        userController.userDelete
    );
};
