/**
 * Auth Service
 */
import bcrypt from 'bcrypt';
import { Repository, getManager } from 'typeorm';
import { UserEntity } from '../users/entity/user';
import jwt from 'jsonwebtoken';
import { JwtSecret, TokenPayload } from './interface/jwt.config';
import smtpTransport from '../custom/smtp';

export class AuthService {
    userRepository: Repository<UserEntity>;

    constructor() {
        this.userRepository = getManager().getRepository(UserEntity);
    }

    public async createToken(user: UserEntity): Promise<any> {
        const expires = '24h'; // an hour
        const tokenPayload: TokenPayload = {
            id: user.id,
            email: user.email,
            role: user.role,
        };
        const token = jwt.sign(tokenPayload, JwtSecret.secret, { expiresIn: expires });
        return token;
    }

    public async generateEmailVerificationToken(email: string): Promise<any> {
        let token = await bcrypt.hash(email, 10);
        return token.replace(/\//g, '');
    }

    public async generateResetPasswordToken(email: string): Promise<any> {
        let token = await bcrypt.hash(email, 10);
        return token.replace(/\//g, '');
    }

    public async comparePassword(inputPass: string, presentPass: string): Promise<any> {
        return await bcrypt.compare(inputPass, presentPass);
    }

    public async sendAccountVerificationEmail(email: string, verificationToken: string, userId: number) {
        try {
            // winston.info(`sendAccountCreationEmail() started for email: ${email}, firstName: ${firstName}, lastName: ${lastName}`);
            const data = {
                to: email,
                from: process.env.No_REPLY_EMAIL,
                template: 'account-verification-email',
                subject: `Confirm Your Email Address! `,
                context: {
                    verificationUrl: `${process.env.SERVER_BASE_URL}/auth/verify/${verificationToken}/${userId}`,
                },
            };
            smtpTransport.sendMail(data);
            // winston.info(`sendAccountCreationEmail() ended for email: ${email}, firstName: ${firstName}, lastName: ${lastName}`);
        } catch (err) {
            console.log(`Exception occurred for sendAccountCreationEmail,-> Error [${err}]`);
        } finally {
            smtpTransport.close();
        }
    }

    public async sendForgetPasswordEmail(email: string, resetPasswordToken: string, userId: number) {
        try {
            // winston.info(`sendAccountCreationEmail() started for email: ${email}, firstName: ${firstName}, lastName: ${lastName}`);
            const data = {
                to: email,
                from: process.env.No_REPLY_EMAIL,
                template: 'forget-password-email',
                subject: `Reset Password! `,
                context: {
                    resetPasswordnUrl: `${process.env.SERVER_BASE_URL}/auth/reset-password/${resetPasswordToken}/${userId}`,
                },
            };
            smtpTransport.sendMail(data);
            // winston.info(`sendAccountCreationEmail() ended for email: ${email}, firstName: ${firstName}, lastName: ${lastName}`);
        } catch (err) {
            console.log(`Exception occurred for sendAccountCreationEmail,-> Error [${err}]`);
        } finally {
            smtpTransport.close();
        }
    }
}
