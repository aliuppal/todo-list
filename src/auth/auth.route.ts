import { AuthController } from './auth.controller';
import { Auth } from '../custom/auth';
import authValMiddleware from './auth.middleware';
import { errorMiddleware } from '../custom/error-middleware';
import { Application } from 'express';

const authControlller = new AuthController();
const authMiddleware = new Auth();

const resource = '/auth';

module.exports = (app: Application, version: string) => {

    app.post(
        `${version}${resource}/register`,
        authControlller.userCreate,
        errorMiddleware
    );
    app.post(
        `${version}${resource}/login`,
        authControlller.userLogin,
        errorMiddleware
    );
    app.get(
        `${version}${resource}/status`,
        authMiddleware.verifyUser,
        authControlller.authStatus,
        errorMiddleware
    );
    app.get(
        `${version}${resource}/admin`,
        authMiddleware.verifyUser,
        authMiddleware.roleAuthorization,
        authControlller.authStatus,
        errorMiddleware
    );
    app.post(
        `${version}${resource}/forget-password`,
        authControlller.userForgetPassword,
        errorMiddleware
    );
    app.post(
        `${version}${resource}/reset-password/:token/:userId`,
        authControlller.userResetPassword,
        errorMiddleware
    );

    app.get(
        `${version}${resource}/verify/:token/:userId`,
        authControlller.userVerifyEmail,
        errorMiddleware
    );
    app.post(
        `${version}${resource}/resend-verification-email`,
        authControlller.resendVerificationEmail,
        errorMiddleware
    );
};
