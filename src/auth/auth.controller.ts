/**
 * Auth Controller
 */
import bcrypt from 'bcrypt';
import { Request, Response, NextFunction } from 'express';
import { CreateUserDto } from '../users/dto/user.dto';
import { UsersService } from '../users/user.service';
import { LoginDto } from './dto/login.dto';
import { AuthService } from './auth.service';
import { CreateAccountException, UserNotFoundException, InvalidEmailPassowrdException, UserNotVerifiedException } from '../custom/error-middleware';

export class AuthController {

    public usersService = new UsersService();
    public authService = new AuthService();

    public userCreate = async (request: any, response: any, next: NextFunction) => {
        const userData: CreateUserDto = request.body;
        try {

            const user: any = await this.usersService.createUser(userData);
            if (user) {
                this.authService.sendAccountVerificationEmail(user.email, user.verificationToken, user.id);
                response.status(201).send({ msg: 'Registration complete!, Please verify your email to continue' });
            }
        } catch (error) {
            next(new CreateAccountException());
        }
    };

    public userLogin = async (request: Request, response: Response, next: NextFunction) => {
        const logInDto: LoginDto = request.body;
        const user = await this.usersService.getUsersByEmail(logInDto.email);
        if (user) {
            if (!user.isVerified) {
                next(new UserNotVerifiedException());
                return;
            }
            const passMatch = await this.authService.comparePassword(logInDto.password, user.password);
            if (passMatch) {
                const userToken = await this.authService.createToken(user);
                response.status(200).send({ token: userToken });
            } else {
                next(new InvalidEmailPassowrdException());
            }
        } else {
            next(new InvalidEmailPassowrdException());
        }
    };

    public userForgetPassword = async (request: Request, response: Response, next: NextFunction) => {
        const email: string = request.body.email;
        const user = await this.usersService.getUsersByEmail(email);
        if (user) {
            const updatedUserData: any = {};
            updatedUserData.resetPasswordToken = await this.authService.generateEmailVerificationToken(user.email);
            updatedUserData.resetPasswordExpires = new Date();
            await this.usersService.updateUser(user.id, updatedUserData);
            this.authService.sendForgetPasswordEmail(user.email, updatedUserData.resetPasswordToken, user.id);
            response.status(200).send({ msg: 'Password reset email sended!' });
        } else {
            next(new UserNotFoundException());
        }
    };

    public userResetPassword = async (request: Request, response: Response, next: NextFunction) => {
        const userId: number = +request.params.userId;
        const resetPasswordToken: string = request.params.token;
        const newPassword: string = request.body.password;
        const user = await this.usersService.getUserByResetPasswordToken(userId, resetPasswordToken);
        if (user) {
            const updatedUserData: any = {};
            updatedUserData.password = await bcrypt.hash(newPassword, 10);
            updatedUserData.resetPasswordToken = null;

            await this.usersService.updateUser(user.id, updatedUserData);
            response.status(200).send({ msg: 'Password successfully updated!' });
        } else {
            next(new UserNotFoundException());
        }
    };

    public resendVerificationEmail = async (request: Request, response: Response, next: NextFunction) => {
        const email: string = request.body.email;
        const user = await this.usersService.getUsersByEmail(email);
        if (user) {
            const updatedUserData: any = {};
            updatedUserData.verificationToken = await this.authService.generateEmailVerificationToken(user.email);
            await this.usersService.updateUser(user.id, updatedUserData);
            this.authService.sendAccountVerificationEmail(user.email, updatedUserData.verificationToken, user.id);
            response.status(200).send({ msg: 'Verification email sended.' });
        } else {
            next(new UserNotFoundException());
        }
    };

    public userVerifyEmail = async (request: Request, response: Response, next: NextFunction) => {
        const userId: number = +request.params.userId;
        const verificationToken: string = request.params.token;
        let user: any = await this.usersService.getUserByEmailVerificationToken(userId, verificationToken);
        if (user) {
            const updatedUserData: any = {};
            updatedUserData.isVerified = true;
            updatedUserData.verificationToken = null;
            await this.usersService.updateUser(user.id, updatedUserData);
            response.status(200).send({ msg: 'Email successfully verified.' });
        } else {
            next(new UserNotFoundException());
        }
    };

    public authStatus = async (req: any, res: Response) => {
        const user = await req.user;
        res.status(200).send(user);
    };

    public authRoleCheck = async (req: any, res: Response) => {
        const user = await req.user;
        res.status(200).send(user);
    };
}
