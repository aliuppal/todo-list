import { Application } from 'express';
import glob from 'glob';
import { constants, logger } from '../custom';

const routes = (app: Application) => {

    const version = `/api/${constants.apiVersion}`;

    logger.info('Routes Loading Started...');

    const routesPath = 'src/**/*.route.ts';
    const files = glob.sync(routesPath);       
    files.forEach((file) => {
        require(`../../${file}`)(app,version);
        logger.info(`'${file}' is loaded`);
    });

    logger.info('Routes Loading Completed...');
};

export default routes;