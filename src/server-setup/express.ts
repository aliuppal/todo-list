import cors from 'cors';
// import * as helmet from "helmet";
import express from 'express';

import { CronJob } from '../custom/cron-job';
import '../custom/exceptions';

import { errorMiddleware } from '../custom/error-middleware';
import { logger } from '../custom';
import routes from './routes';

export class ExpressApp {
    public app: express.Application;
    public port: number;
    public middlewares: any;
    public cronJob = new CronJob();

    constructor() {
        this.app = express();
        this.port = Number(process.env.SERVER_PORT) || 3000;
       
        this.initializeMiddlewares();
        this.initializeRoutes();
        this.initializeErrorHandling();

        this.handleCORSErrors();
    }

    public listen() {
        this.app.listen(this.port, () => {
            this.cronJob.sendTasksDailyReport();

            logger.info(`App listening on the port ${this.port}`);
        });
    }

    private initializeMiddlewares() {
        const corsOptions = {
            origin: 'http://localhost:4200',
            optionSuccessStatus: 200,
        };

        this.app.use(express.json()), this.app.use(express.urlencoded({ extended: true }));
        this.app.use(cors(corsOptions));
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware);
    }

    private initializeRoutes() {
        routes(this.app);
    }

    private handleCORSErrors(): any {
        this.app.use((req: any, res: any, next: any) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-ALlow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
            if (req.method === 'OPTIONS') {
                res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, GET, DELETE');
                return res.status(200).json({});
            }
            next();
        });
    }
}
