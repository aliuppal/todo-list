/**
 Copyright (c) 2019, Zigron Inc, https://zigron.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

import logger from './logger';

// const emailService = require('../modules/common/email.service');

process.on('uncaughtException', (err: any) => {
	const context = {
		name: 'uncaughtException',
		server: process.env.NODE_ENV,
		message: JSON.stringify(err),
		stack: err.stack
	};
	logger.error(JSON.stringify(context));
	try {
		// emailService.sendExceptionEmail(context);
	} catch (e) {
		// winston.error(`Error while sending email ${e.message}`);
		logger.error(e);
	}
});

process.on('unhandledRejection', (err: any) => {
	const context = {
		name: 'unhandledRejection',
		server: process.env.NODE_ENV,
		message: JSON.stringify(err),
		stack: err.stack
	};
	logger.error(JSON.stringify(context));
	try {
		// emailService.sendExceptionEmail(context);
	} catch (e) {
		// winston.error(`Error while sending email ${e.message}`);
		logger.error(e);
	}
});
