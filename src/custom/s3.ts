import { logger } from './';
import * as fs from "fs";
import * as AWS from "aws-sdk";

// Set the Region
AWS.config.update({ region: 'us-east-1' });

// The name of the bucket that you have created
const BUCKET_NAME = process.env.S3_BUCKET;
const FILE_PERMISSION = 'public-read'

const s3Bucket = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
});

// CHECK IF AWS S3 BUCKET EXIST
s3Bucket.headBucket({ Bucket: `${BUCKET_NAME}`}, (err: any, exists: any) => {
    if (err) {
        return logger.error(`Error while checking for bucket [${BUCKET_NAME}] -> ${err}`);
    } else if (exists) {
        return logger.info(`[${BUCKET_NAME}] Bucket exists`);
    }
    return createBucket();
});

// CREATE AWS S3 BUCKET
const createBucket = () => {
    s3Bucket.createBucket({ Bucket: `${BUCKET_NAME}` }, (err: any) => {
        if (err) return logger.error(`[${BUCKET_NAME}] S3 Bucket could not be created: [${err}]`);
        return logger.info(`[${BUCKET_NAME}] S3 Bucket created Successfully`);
    });
};

// UPLOAD FILE TO AWS S3 BUCKET
const uploadFile = (file: any, fileName: string) =>
    new Promise((resolve, reject) => {

        // Read content from the file
        const readStream = fs.createReadStream(file);

        // Setting up S3 upload parameters
        const uploadParams : any= {
            Bucket: BUCKET_NAME,
            Key: fileName, // File name you want to save as in S3
            Body: readStream,
            ACL: FILE_PERMISSION
        };

        // Uploading files to the bucket
        s3Bucket.upload(uploadParams, (err: any, res: any) => {
            if (err) {
                logger.error(`Error while uploading file to S3 Bucket: [${err}]`);
                return reject(err);
            }
            logger.info(`File uploaded successfully to S3 Bucket: [${BUCKET_NAME}] [${fileName}]`);
            return resolve(res);
        });
    });

// REMOVE FILE FROM AWS S3 BUCKET
const removeFile = (fileName: string) => {
    // Setting up S3 parameters
    const params: any = {
        Bucket: BUCKET_NAME,
        Key: fileName, // File name saved in S3
    };
    // Deleting file from the bucket
    s3Bucket.deleteObject(params, (err: any) => {
        if (err) {
            logger.error(`Error while Removing File from S3 Bucket: [${BUCKET_NAME}] [${fileName}] -> [${err}]`);
            return err;
        }
        return logger.info(`File Removed Successfully from S3 Bucket: [${BUCKET_NAME}] [${fileName}]`);
    });
};

// GETTING SIGNED URL FOR S3 BUCKET PRIVATE FILE
const getFileSignedURL = async(fileName: string) => {
    const signedUrlExpireSeconds = 24 * 30 * 3600; // 30 days
    // Setting up S3 parameters
    const params: any = {
        Bucket: BUCKET_NAME,
        Key: fileName, // File name saved in S3
        Expires: signedUrlExpireSeconds,
    };
    const signedURL = s3Bucket.getSignedUrl('getObject', params);
    logger.info(`S3 Bucket Signed URL for file ${fileName} is: ${signedURL}`);

    return signedURL;
};

export {
    uploadFile,
    removeFile,
    getFileSignedURL
};
