/**
 * Auth MiddleWare
 */
 import { Router, Request, Response, NextFunction } from "express";
 import * as jwt from "jsonwebtoken";
 import { UserNotFoundException, UnathorizedException, UserNotVerifiedException, ForbidenException } from '../custom/error-middleware';
 import { JwtSecret } from "../auth/interface/jwt.config";
 import { UsersService } from '../users/user.service';
 
 export class Auth {
 
     public usersService = new UsersService();
 
     verifyUser = async (req: any, res: any, next: NextFunction) => {
         try {
             const token = req.header("Authorization")?.replace("Bearer ", "");
             const verifyRes: any = jwt.verify(token, JwtSecret.secret);
             const user = await this.usersService.getUsersByEmail(verifyRes.email);
             if (!user) {
                return new UnathorizedException();
             }
             if (!user.isVerified) {
                return next(new UserNotVerifiedException());
             }
             req.token = token;
             req.user = user;
             return next();
         } catch (error) {
            return next(new UnathorizedException());
         }
     };
      
     roleAuthorization = (req: any, next: any) => {
         if (req.user.role !== "admin") {
             throw new ForbidenException();
         } else {
             next();
         }
     };
 };