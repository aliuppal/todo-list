import * as winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import path from 'path';
import fs from 'fs';

const logDirectory = './logs';

// Create log directory if it does not exist.
if (!fs.existsSync(logDirectory)) {
	fs.mkdirSync(logDirectory);
}

const logFile = path.join(logDirectory, 'server-logs');

// Defining custom logging format
const customFormat = winston.format.printf(({
	level,
	message,
	timestamp,
}) => (`${timestamp} ${level}: ${message}`));


const transport: DailyRotateFile = new DailyRotateFile({
	filename: `${logFile}-%DATE%.log`,
	datePattern: 'YYYY-MM-DD-HH',
	zippedArchive: true,
	maxFiles: '30d'
});
  
// Creating logger object
const logger = winston.createLogger({
	format: winston.format.combine(
		winston.format.colorize(),
		winston.format.label(),
		winston.format.timestamp({
			format: 'YYYY-MM-DD HH:mm:ss',
		}),
		customFormat,
	),
	transports: [
		new winston.transports.Console(),
		transport
	]
});

logger.on('error', (error) => {
	console.error('Error while trying to write to graylog2:', error);
});
export default logger;
