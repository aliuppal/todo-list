import { Request, Response, NextFunction } from "express";
import { validationResult } from 'express-validator';

const errorFormatter = ({ msg } : any) => msg;

export const errorMiddleware = (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
        const err = errors.mapped();
        return next({ name: 'ValidationError', errors: err, status: 400 });
    }
    return next();
};

export class HttpException extends Error {
    public status: number;
    public message: string;
    constructor(status: number, message: string) {
        super(message);
        this.status = status;
        this.message = message;
    }
}

export class UserNotFoundException extends HttpException {
    constructor() {
        super(404, 'The user does not exist');
    }
}

export class UserNotVerifiedException extends HttpException {
    constructor() {
        super(404, 'Please verify you email to continue');
    }
}

export class ComparePasswordException extends HttpException {
    constructor() {
        super(401, 'Failed to compare password');
    }
}

export class InvalidEmailPassowrdException extends HttpException {
    constructor() {
        super(401, 'Invalid email or password!');
    }
}

export class UnathorizedException extends HttpException {
    constructor() {
        super(401, 'Your no authorized, login to continue');
    }
}

export class CreateAccountException extends HttpException {
    constructor() {
        super(400, 'Failed to create account, Try again Letter');
    }
}

export class ForbidenException extends HttpException {
    constructor() {
        super(505, 'Your Forbidden to Access Resources Requested');
    }
}
