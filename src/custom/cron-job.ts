import * as schedule from "node-schedule";
import { UsersService } from "../users/user.service";

export class CronJob {

    //runs at 12:00 AM
    public sendTasksDailyReport() {
        const rule = new schedule.RecurrenceRule();
        rule.dayOfWeek = [0, new schedule.Range(0, 6)];
        rule.hour = 18;
        rule.minute = 0;
        
        const usersService = new UsersService();
    
        schedule.scheduleJob(rule, async function () {
         
            // logic add here
            usersService.generateUsersDailyReport();


            const date = new Date()
            console.log(`Job run after every ---------> ${date}`);

        });
    }
}