import nodemailer from 'nodemailer';
import hbs from 'nodemailer-express-handlebars';

const email = process.env.No_REPLY_EMAIL 
const password = process.env.NO_REPLY_PASSWORD 

const smtpTransport = nodemailer.createTransport({
	service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
	auth: {
		user: email,
		pass: password
	}
});

const handlebarsOptions: any = {
	viewEngine: {
		extName: '.hbs',
		defaultLayout: null,
		partialsDir: (`${__dirname}/../../templates/`),
		layoutsDir: (`${__dirname}/../../templates/`)
	},
	viewPath: (`${__dirname}/../../templates/`),
	extName: '.hbs'
};

smtpTransport.use('compile', hbs(handlebarsOptions));

export default smtpTransport;
