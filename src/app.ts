import { createConnection } from 'typeorm';
import { ExpressApp } from './server-setup/express';

createConnection()
    .then(async () => {
        const application = new ExpressApp();
        
        application.listen();
    })
    .catch((_error) => {
        console.log(`error: While createConnection ${_error}`);
    });
