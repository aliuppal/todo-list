/**
 * Task Status
 */

export enum TaskStatus {
    TODO = 'todo',
    IN_PROGRESS = 'inprogress',
    DONE = 'done',
}
