/**
 * User Entity
 */
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne
} from "typeorm";
import { UserEntity } from "../../users/entity/user";
import { TaskStatus } from "./task-status";

@Entity('tasks')
export class TasksEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", nullable: false })
    title: string;

    @Column({ type: "text", nullable: false })
    description: string;

    @Column({ type: "text", nullable: true })
    file: string;
    
    @Column({ type: "varchar", default: TaskStatus.TODO })
    status: TaskStatus;

    @Column({ type: "date", nullable: true })
    dueAt: Date;

    @Column({ type: "date", nullable: true })
    doneAt: Date;

    @Column({ type: "boolean", nullable: false, default: false })
    isDeleted: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(() => UserEntity, (user: UserEntity) => user.tasks, { eager: false })
    user: UserEntity;

    constructor(
        id: number,
        title: string,
        description: string,
        file: string,
        status: TaskStatus,
        dueAt: Date,
        doneAt: Date,
        isDeleted: boolean,
        createdAt: Date,
        updatedAt: Date,
        user: UserEntity
    ) {
        (this.id = id),
            (this.title = title),
            (this.description = description),
            (this.file = file),
            (this.status = status),
            (this.dueAt = dueAt),
            (this.doneAt = doneAt),
            (this.isDeleted = isDeleted),
            (this.createdAt = createdAt),
            (this.updatedAt = updatedAt),
            (this.user = user);
    }
}
