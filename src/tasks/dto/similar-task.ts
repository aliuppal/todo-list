/**
 * CreateTaskDto
 */

import { IsString, IsNotEmpty, IsDate, IsOptional } from 'class-validator';

export class SimilarTaskDto {
   
    @IsOptional()
    title: string;

    @IsOptional()
    userId: number;

    @IsOptional()
    taskId: number;

    constructor(title: string, userId: number, taskId: number) {
        this.title = title;
        this.userId = userId;
        this.taskId = taskId;
    }
}
