import { IsIn, IsNotEmpty, IsOptional } from 'class-validator';
import { TaskStatus } from '../entity/task-status';

export class TasksFilterDto {
  @IsOptional()
  @IsIn([TaskStatus.TODO, TaskStatus.IN_PROGRESS, TaskStatus.DONE])
  status: TaskStatus;

  @IsOptional()
  @IsNotEmpty()
  search: string;
}
