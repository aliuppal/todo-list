/**
 * CreateTaskDto
 */

import { IsString, IsNotEmpty, IsDate, IsOptional } from 'class-validator';

export class CreateTaskDto {
   
    @IsString()
    @IsNotEmpty()
    title: string;
    
    @IsString()
    @IsNotEmpty()
    description: string;

    @IsString()
    @IsOptional()
    file: string;

    @IsDate()
    @IsNotEmpty()
    dueAt: Date;

    constructor(title: string, description: string, file: string, dueAt: Date) {
        this.title = title;
        this.description = description;
        this.file = file;
        this.dueAt = dueAt;
    }
}
