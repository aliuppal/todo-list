/**
 * Task Controller
 */

import { Router, NextFunction, Request, Response } from 'express';
import { TaskService } from './task.service';
import { TasksEntity } from './entity/task';
import { CreateTaskDto } from './dto/create-task.dto';
import { HttpException } from '../custom/error-middleware';
import { Auth } from '../custom/auth';
import { TasksFilterDto } from './dto/tasks-filter.dto';
import { SimilarTaskDto } from './dto/similar-task';

export class TaskController {
    public taskService = new TaskService();

    public getTasks = async (req: any, res: any, next: NextFunction) => {
        const filtertDto: TasksFilterDto = req.query;
        const userId: number = +req.user.id;
        const task: TasksEntity[] | undefined = await this.taskService.getTasks(filtertDto, userId);
        res.status(200).send(task);
    };

    public tasksCreate = async (req: any, res: any, next: NextFunction) => {
        const taskDto: CreateTaskDto = req.body;
        try {
            const task = await this.taskService.createTask(taskDto, req.file, req.user);
            delete task.user;

            const similarTaskDto: SimilarTaskDto = {title: task.title, taskId: task.id, userId: req.user.id }
            const similarTasks = await this.taskService.getSimilarTasks(similarTaskDto);

            res.status(200).send({task, similarTasks});
        } catch (error) {
            next(error);
        }
    };

    public taskById = async (req: any, res: any, next: NextFunction) => {
        try {
            const taskId: number = +req.params.taskId;
            const userId: number = +req.user.id;
            const task = await this.taskService.findTaskById(taskId, userId);
            if (!task) {
                next(new Error('The task not found'));
            }
            res.status(200).send(task);
        } catch (error) {
            next(new Error('Error while fetching task'));
        }
    };

    public taskUpdate = async (req: any, res: any) => {
        try {
            const taskId: number = +req.params.taskId;
            const userId: number = +req.user.id;
            const taskDto: TasksEntity = req.body;

            const taskRes = await this.taskService.updateTask(taskId, taskDto, req.file, userId);
            if (taskRes) {
                res.status(200).send('Task updated successfully.');
            } else {
                throw new HttpException(400, 'Error Occured while updating task');
            }
        } catch (error) {
            throw new HttpException(400, 'Error Occured while updating task');
        }
    };

    public taskDelete = async (req: any, res: any, next: NextFunction) => {
        const id: number = +req.params.taskId;
        const userId = +req.user.id;
        const taskUser = await this.taskService.findTaskById(id, userId);
        if (!req.user.email === taskUser.user.email) {
            throw new HttpException(401, 'Your are not authorized');
        }
        const task = await this.taskService.deleteTaskById(id);
        if (task) {
            res.status(200).send('Task deleted successfully');
        } else {
            next(new Error('Failed to delete user'));
        }
    };

    public countTasksByStatus = async (req: any, res: any, next: NextFunction) => {
        try {
            const userId = +req.user.id;
            const taskCount = await this.taskService.countTasksByStatus(userId);
            res.status(200).send(taskCount);
        } catch (error) {
            next(new Error('Error while fetching tasks count'));
        }
    };

    public tasksCompletedAfterDueDate = async (req: any, res: any, next: NextFunction) => {
        try {
            const userId = +req.user.id;
            const taskCount = await this.taskService.tasksCompletedAfterDueDate(userId);
            res.status(200).send({ taskCount });
        } catch (error) {
            next(new Error('Error while fetching tasks count'));
        }
    };

    public tasksDueToday = async (req: any, res: any, next: NextFunction) => {
        try {
            const userId = +req.user.id;
            const taskCount = await this.taskService.tasksDueToday(userId);
            res.status(200).send({ taskCount });
        } catch (error) {
            next(new Error('Error while fetching due tasks'));
        }
    };

    public maxTasksCompletedOnSingleDay = async (req: any, res: any, next: NextFunction) => {
        try {
            const userId = +req.user.id;
            const taskCount = await this.taskService.maxTasksCompletedOnSingleDay(userId);
            res.status(200).send({ taskCount });
        } catch (error) {
            next(new Error('Error while fetching max Tasks Completed OnSingle Day'));
        }
    };

    public getDailyAverageCompletedTasks = async (req: any, res: any, next: NextFunction) => {
        try {
            const averageDailyTasks = await this.taskService.getDailyAverageCompletedTasks(req.user);
            res.status(200).send({ averageDailyTasks });
        } catch (error) {
            next(new Error('Error while fetching Daily Average Completed Tasks'));
        }
    };
    
    public getSimilarTasks = async (req: any, res: any, next: NextFunction) => {
        try {
            const similarTaskDto: SimilarTaskDto = {title: req.body.title, taskId: req.body.taskId, userId: req.user.id }
            const similarTasks = await this.taskService.getSimilarTasks(similarTaskDto);
            res.status(200).send({ similarTasks });
        } catch (error) {
            next(new Error('Error while fetching Similar Tasks '));
        }
    };
}
