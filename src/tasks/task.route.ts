import multer from "multer";
import { TaskController } from './task.controller';
import { Auth } from '../custom/auth';
import taskValMiddleware from './task.middleware';
import { errorMiddleware } from '../custom/error-middleware';
import { Application } from 'express';

const storage = multer.memoryStorage();
const imageUpload = multer({ storage });

const mediaFields = [
	{ name: "images", maxCount: 20 },
	{ name: "audios", maxCount: 20 },
	{ name: "videos", maxCount: 20 },
];

const taskController = new TaskController();
const authMiddleware = new Auth();

const resource = '/tasks';

module.exports = (app: Application, version: string) => {
    app.get(
        `${version}${resource}/status/count`,
        authMiddleware.verifyUser,
        taskController.countTasksByStatus,
        errorMiddleware
    );
    app.get(
        `${version}${resource}/complete-after-due`,
        authMiddleware.verifyUser,
        taskController.tasksCompletedAfterDueDate,
        errorMiddleware
    );
    app.get(
        `${version}${resource}/due-today`,
        authMiddleware.verifyUser,
        taskController.tasksDueToday,
        errorMiddleware
    );
    app.get(
        `${version}${resource}/most-in-a-day`,
        authMiddleware.verifyUser,
        taskController.maxTasksCompletedOnSingleDay,
        errorMiddleware
    );
    app.get(
        `${version}${resource}/daily-average`,
        authMiddleware.verifyUser,
        taskController.getDailyAverageCompletedTasks,
        errorMiddleware
    );
    app.get(
        `${version}${resource}`,
        authMiddleware.verifyUser,
        taskController.getTasks,
        errorMiddleware
    );
    app.get(
        `${version}${resource}/:taskId`,
        authMiddleware.verifyUser,
        taskController.taskById,
        errorMiddleware
    );
    app.post(
        `${version}${resource}`,
        authMiddleware.verifyUser,
        taskController.tasksCreate,
        imageUpload.fields(mediaFields),
        errorMiddleware
    );
    app.patch(
        `${version}${resource}/:taskId`,
        authMiddleware.verifyUser,
        taskController.taskUpdate,
        imageUpload.fields(mediaFields),
        errorMiddleware
    );
    app.delete(
        `${version}${resource}/:taskId`,
        authMiddleware.verifyUser,
        taskController.taskDelete,
        errorMiddleware
    );
};
