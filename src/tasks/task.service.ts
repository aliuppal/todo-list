/**
 * Task Service
 */

 import { v4 as uuidv4 } from 'uuid';
import { getManager, Repository } from 'typeorm';
import { TasksEntity } from './entity/task';
import { CreateTaskDto } from './dto/create-task.dto';
import { UserEntity } from '../users/entity/user';
import { logger } from '../custom';
import { TasksFilterDto } from './dto/tasks-filter.dto';
import moment from 'moment'
import { TaskStatus } from './entity/task-status';
import { uploadFile, removeFile } from '../custom/s3';
import { SimilarTaskDto } from './dto/similar-task';

export class TaskService {
    taskRepository: Repository<TasksEntity>;

    constructor() {
        this.taskRepository = getManager().getRepository(TasksEntity);
    }

    public async getTasks(filtertDto: TasksFilterDto, userId: number): Promise<TasksEntity[] | undefined> {

        let tasks: TasksEntity[] = [];
        try {
            logger.info(filtertDto);

            const { status, search } = filtertDto;
            const query = this.taskRepository.createQueryBuilder('task');

            if (userId) {
                query.where('task.userId = :userId', { userId });
            }
            
            query.andWhere('task.isDeleted = :isDeleted', { isDeleted: false });

            if (status) {
                query.andWhere('task.status = :status', { status });
            }
            if (search) {
                query.andWhere('task.title LIKE :search OR task.description LIKE :search', { search: `%${search}%` });
            }

            tasks = await query.getMany();
            logger.info(tasks);
           
        } catch (error) {
            logger.error(error);
        }
        return tasks;
    }

    public async createTask(taskDto: CreateTaskDto, file: any, userEntity: UserEntity): Promise<any> {
        try {

            if (file) {
                const imageFile = `${uuidv4()}_${file.originalname}`;
                await uploadFile(file, imageFile);
    
                taskDto.file = `${process.env.S3_ENDPOINT}/${process.env.S3_ENDPOINT}/${imageFile}`;
            }

            const task = this.taskRepository.create({
                ...taskDto,
                user: userEntity,
            });
            const TaskRes = await this.taskRepository.save(task);
        
            return TaskRes;
        } catch (except) {
            throw new Error('Error while creating task');
        }
    }

    async findTaskByTitle(title: string): Promise<TasksEntity[]> {
        return this.taskRepository.find({ where: { title: title }, relations: ['user'] });
    }

    async findTaskById(taskId: number, userId: number): Promise<TasksEntity | any> {
        return await this.taskRepository.findOne({ where: { id: taskId, user: { id: userId } } });
    }

    async updateTask(taskId: number, tasksEntity: TasksEntity, file: any, userId: number): Promise<any> {
        const task = await this.findTaskById(taskId, userId);
        if (!task) {
            throw new Error('The task not found');
        } else {
            try {
                if (file) {
                    const imageFile = `${uuidv4()}_${file.originalname}`;
                    await uploadFile(file, imageFile);
        
                    const oldImage = task.file;
                    tasksEntity.file = `${process.env.S3_ENDPOINT}/${process.env.S3_ENDPOINT}/${imageFile}`;
                    oldImage && removeFile(oldImage);
                }

                return await this.taskRepository.update(taskId, tasksEntity);
            } catch (error) {
                throw new Error('User Update Error');
            }
        }
    }

    async deleteTaskById(taskId: number): Promise<any> {
        this.taskRepository.delete(taskId);
    }

    async countTasksByStatus(userId: number): Promise<any> {
        return await this.taskRepository.query(`SELECT status, COUNT(id) as taskCount FROM tasks WHERE userId = ${userId} GROUP BY status`);
    }

    async getDailyAverageCompletedTasks(user: UserEntity): Promise<any> {

        var noOfDays: number = moment().diff(user.createdAt, 'days');
        const totalCompletedTasks = await this.taskRepository.query(`SELECT COUNT(*) FROM tasks WHERE "status" = '${TaskStatus.DONE}' AND "userId" = ${user.id}`);
    
        const averageDailyTasks = (totalCompletedTasks[0].count / noOfDays).toFixed(2);
        
        return averageDailyTasks;
    }

    async tasksCompletedAfterDueDate(userId: number): Promise<any> {
        return await this.taskRepository.query(`SELECT * FROM tasks WHERE "doneAt" > "dueAt" AND "userId" = ${userId} ORDER BY id desc`);
    }

    async maxTasksCompletedOnSingleDay(userId: number): Promise<any> {
        return await this.taskRepository.query(`SELECT cast("doneAt" as date), COUNT(id) as completedTasksCount FROM tasks WHERE "userId" = ${userId} GROUP BY cast("doneAt" as date) ORDER BY cast("doneAt" as date) LIMIT 1`);
    }

    async tasksCreatedWeekly(): Promise<any> {
        // this.taskRepository.delete(id);
    }

    async tasksDueToday(userId: number): Promise<any> {
        let tasks: TasksEntity[] = [];
        try {

            const currentDate = moment(new Date()).format('YYYY-MM-DD');

            const query = this.taskRepository.createQueryBuilder('task');
            query.where('task.status = :todo', { todo: TaskStatus.TODO });
            query.orWhere('task.status = :inprogress', { inprogress: TaskStatus.IN_PROGRESS });
            query.where('task.dueAt = :start', { start: currentDate });
            query.andWhere('task.userId = :userId', { userId });
            query.andWhere('task.isDeleted = :isDeleted', { isDeleted: false });
            tasks = await query.getMany();
            
            logger.info(tasks);
           
        } catch (error) {
            logger.error(error);
        }
        return tasks;
    }

    async getSimilarTasks(similarTaskDto: SimilarTaskDto): Promise<any> {
        let tasks: TasksEntity[] = [];
        try {

            const query = this.taskRepository.createQueryBuilder('task');
            if (similarTaskDto.taskId) {
                query.where('task.title LIKE :search', { search: `%${similarTaskDto.title}%` });
            }
            if (similarTaskDto.taskId) {
                query.andWhere('task.id != :taskId', { taskId: similarTaskDto.taskId });
            }
            
            query.andWhere('task.userId = :userId', { userId: similarTaskDto.userId });
            query.andWhere('task.isDeleted = :isDeleted', { isDeleted: false });
            tasks = await query.getMany();
            
            logger.info(`getSimilarTasks: ${tasks}`);
           
        } catch (error) {
            logger.error(error);
        }
        return tasks;
    }
    
}
